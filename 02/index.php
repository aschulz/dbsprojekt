<html>
<head>
<title>First Query</title>
</head>
<body>

<?php
$conn_string = "host=localhost port=5433 dbname=dbs user=testuser password=testpass";

$conn = pg_pconnect($conn_string);


if (!$conn) {
  echo "Ein Fehler ist aufgetreten.1\n";
  exit;
}
$result = pg_query($conn, "SELECT * FROM \"student\"");
if (!$result) {
  echo "Ein Fehler ist aufgetreten.2\n";
  exit;
}
while ($row = pg_fetch_row($result)) {
  echo "Matrikelnummer ".$row[0]."  Vorname: ".$row[1]."   Nachname: ".$row[2];
  echo "<br />\n";
}
pg_close($conn);
?>
</body>
</html>
