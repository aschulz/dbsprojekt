CREATE TABLE "User" (
    uid serial NOT NULL PRIMARY KEY,
    username varchar(30) NOT NULL UNIQUE
);

CREATE TABLE "Tweet" (
    tid integer NOT NULL PRIMARY KEY,
    uid integer NOT NULL REFERENCES "User",
    tweetText varchar(140) NOT NULL,
    favCounter integer CHECK (favCounter>0),
    retweetCounter integer CHECK (retweetCounter>0),
    tweetTime timestamp NOT NULL 
);

CREATE TABLE "Link" (
    lid serial NOT NULL PRIMARY KEY,
    linkURL varchar(140) UNIQUE
);

CREATE TABLE "Hashtag" (
    hid serial NOT NULL PRIMARY KEY,
    name varchar(140) NOT NULL UNIQUE
);

CREATE TABLE "HasRetweeted" (
    uid integer NOT NULL REFERENCES "User",
    tid integer NOT NULL REFERENCES "Tweet"
);

CREATE TABLE "MentionedInTweet" (
    tid integer NOT NULL REFERENCES "Tweet",
    uid integer NOT NULL REFERENCES "User"
);

CREATE TABLE "LinkInTweet" (
    tid integer NOT NULL REFERENCES "Tweet",
    lid integer NOT NULL REFERENCES "Link"
);

CREATE TABLE "HashtagInTweet" (
    tid integer NOT NULL REFERENCES "Tweet",
    hid integer NOT NULL REFERENCES "Hashtag"
);