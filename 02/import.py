#! usr/bin/python
# -*- coding: utf-8 -*-

import os
import re
import csv
import psycopg2
import sys

reload(sys)
sys.setdefaultencoding('utf8')

project_location = '/home/carl/Uni/6_Datenbanksysteme/Projekt/'
db_host = "localhost"
db_port = "5432"
db_user = "carl"
db_password = "carl"

# Initialisierung: Löschen alter temporärer Daten im Projektordner und in der Datenbank
def init():
    if os.path.exists(project_location + '02/useful_columns.csv'):
        os.remove(project_location + '02/useful_columns.csv')
    if os.path.exists(project_location + '02/cleaned_data.csv'):
        os.remove(project_location + '02/cleaned_data.csv')
    cnx = psycopg2.connect(user=db_user,
                           host=db_host,
                           database="Election",
                           port=db_port,
                           password=db_password)
    cursor = cnx.cursor()
    drop_tables_query = 'DROP TABLE IF EXISTS "User", "Tweet", "Hashtag", "HashtagInTweet", "Link", "LinkInTweet", "MentionedInTweet", "HasRetweeted" CASCADE;'
    cursor.execute(drop_tables_query)
    cnx.commit()
    for query in open(project_location + '02/create_tables.sql', "r").read().split(';\n'):
        cursor.execute(query)
    cnx.commit()
    cursor.close()
    cnx.close()

# Auswahl der verwendeten Daten gemäß ER und RM und erstellen temporärer CSV-Dateien
def select_useful_columns():
    with open(project_location + 'american-election-tweets.csv', "r") as f:
        reader = csv.reader(f, delimiter=';', quotechar='"')
        for index, line in enumerate(reader):
            if index == 0:
                continue
            author = line[0]
            text = line[1]
            is_retweeted = line[2]
            orig_author = line[3]
            time = line[4]
            retweet_count = line[7]
            fav_count = line[8]
            with open(project_location + '02/useful_columns.csv', "a") as tmp:
                writer = csv.writer(tmp, delimiter=';', quotechar='"')
                writer.writerow([index, author, text, is_retweeted, orig_author, time, retweet_count, fav_count])

# Daten bereinigen
def clean_data():
    with open(project_location + '02/useful_columns.csv', "r") as f:
        reader = csv.reader(f, delimiter=';', quotechar='"')
        for line in reader:
            id = line[0]
            cleaned_author = clean_author(line[1])
            cleaned_text = clean_text(line[2])
            if not cleaned_text:
                continue
            cleaned_is_retweeted = line[3]
            cleaned_orig_author = clean_author(line[4])
            cleaned_time = line[5]
            cleaned_retweet_count = line[6]
            cleaned_fav_count = line[7]
            if cleaned_is_retweeted == 'True':	# Falls is_retweeted = True -> orig_author muss angegeben werden
                if cleaned_orig_author == '':
                    continue
            with open(project_location + '02/cleaned_data.csv', "a") as tmp:
                writer = csv.writer(tmp, delimiter=';', quotechar='"')
                writer.writerow([id, cleaned_author, cleaned_text, cleaned_is_retweeted, cleaned_orig_author, cleaned_time, cleaned_retweet_count, cleaned_fav_count])
    os.remove(project_location +  '02/useful_columns.csv')


def clean_author(author):
    return author.lower()

# Datenbereinigung der Tweettexte insbesondere ASCII -> UTF-8
def clean_text(text):
    cleaned_text = ''
    for char in text:
        try:
            char.encode('utf-8')
            cleaned_text += char
        except:
            if ord(char) in [145, 146]:	#Abfangen von einfachen Anführungszeichen in ASCII und übersetzen in ähnliches UTF-8 Zeichen
                cleaned_text += "'"
            elif ord(char) in [132, 147, 148]:	#Abfangen von doppelten Anführungszeichen in ASCII und übersetzen in ähnliches UTF-8 Zeichen
                cleaned_text += '"'
            else:
                cleaned_text += ' '	# Alle anderen in UTF-8 unbekannte ASCII-Zeichen werden durch ein Leerzeichen ersetzt
    cleaned_text = cleaned_text.replace('&amp;','&')	# HTML-Zeichen werden durch UTF-8 Zeichen ersetzt.
    cleaned_text = cleaned_text.replace('&lt;', '<')
    cleaned_text = re.sub(r'\?{4,}', '', cleaned_text)
    if len(cleaned_text) > 140:	#Falls der bereinigte Tweet-Text mehr als 140 Zeichen hat, wird der Datensatz verworfen.
        return False
    cleaned_text = cleaned_text.replace("'","''")
    return cleaned_text

# Datenimport CSV -> Datenbank "Election"
def import_data():
    cnx = psycopg2.connect(user=db_user,
                           host=db_host,
                           database="Election",
                           port=db_port,
                           password=db_password)
    cursor = cnx.cursor()

    with open(project_location + '02/cleaned_data.csv', "r") as f:
        reader = csv.reader(f, delimiter=';', quotechar='"')
        for index, line in enumerate(reader):
            tid = line[0]
            author = line[1]
            text = line[2]
            user_matches = re.findall(r"@[\w]+[^\w]", text)
            hashtag_matches = re.findall(r"#[\w]+[^\w]", text)
            link_matches = re.findall(r"https://t.co/[\w]{10}", text)
            is_retweeted = line[3] == "True"
            if is_retweeted:
                orig_author = line[4]
            time = line[5].replace('T', ' ')
            retweet_count = line[6]
            fav_count = line[7]

            if is_retweeted:
                db_author = orig_author
            else:
                db_author = author

            #insert orig author
            author_exists_query = 'SELECT uid FROM "User" WHERE username = \'%s\';' % db_author
            cursor.execute(author_exists_query)
            rows = cursor.fetchall()
            if len(rows) == 0:
                author_query = 'INSERT INTO "User" (username) VALUES (\'%s\');' % db_author
                cursor.execute(author_query)
                cnx.commit()
                get_author_id_query = 'SELECT uid FROM "User" WHERE username = \'%s\';' % db_author
                cursor.execute(get_author_id_query)
                rows = cursor.fetchall()
            uid = rows[0][0]

            #insert tweet
            tweet_query = 'INSERT INTO "Tweet" (tid, uid, tweettext, favcounter, retweetcounter, tweettime) ' \
                          'VALUES (%s, %s, \'%s\', %s, %s, \'%s\');' % (tid, uid, text, fav_count, retweet_count, time)
            cursor.execute(tweet_query)
            cnx.commit()

            #insert author
            if is_retweeted:
                author_exists_query = 'SELECT uid FROM "User" WHERE username = \'%s\';' % author
                cursor.execute(author_exists_query)
                rows = cursor.fetchall()
                if len(rows) == 0:
                    author_query = 'INSERT INTO "User" (username) VALUES (\'%s\');' % author
                    cursor.execute(author_query)
                    cnx.commit()
                    get_author_id_query = 'SELECT uid FROM "User" WHERE username = \'%s\';' % author
                    cursor.execute(get_author_id_query)
                    rows = cursor.fetchall()
                uid = rows[0][0]
                retweet_query = 'INSERT INTO "HasRetweeted" (uid, tid) VALUES (%s, %s);' % (uid, tid)
                cursor.execute(retweet_query)
                cnx.commit()

            #insert hashtags
            if len(hashtag_matches) > 0:
                for match in hashtag_matches:
                    match = match[1:-1].lower()
                    hashtag_exists_query = 'SELECT hid FROM "Hashtag" WHERE name = \'%s\';' % match
                    cursor.execute(hashtag_exists_query)
                    rows = cursor.fetchall()
                    if len(rows) == 0:
                        hashtag_query = 'INSERT INTO "Hashtag" (name) VALUES (\'%s\');' % match
                        cursor.execute(hashtag_query)
                        cnx.commit()
                        get_hashtag_id_query = 'SELECT hid FROM "Hashtag" WHERE name = \'%s\';' % match
                        cursor.execute(get_hashtag_id_query);
                        rows = cursor.fetchall()
                    hid = rows[0][0]
                    hashtag_in_tweet_query = 'INSERT INTO "HashtagInTweet" (hid, tid) VALUES (%s, %s);' % (hid, tid)
                    cursor.execute(hashtag_in_tweet_query)
                    cnx.commit()

            #insert mentioned users
            if len(user_matches) > 0:
                for match in user_matches:
                    match = match[1:-1].lower()
                    user_exists_query = 'SELECT uid FROM "User" WHERE username = \'%s\';' % match
                    cursor.execute(user_exists_query)
                    rows = cursor.fetchall()
                    if len(rows) == 0:
                        mentioned_user_query = 'INSERT INTO "User" (username) VALUES (\'%s\');' % match
                        cursor.execute(mentioned_user_query)
                        cnx.commit()
                        get_mentioned_user_id_query = 'SELECT uid FROM "User" WHERE username = \'%s\';' % match
                        cursor.execute(get_mentioned_user_id_query)
                        rows = cursor.fetchall()
                    uid = rows[0][0]
                    user_mentioned_in_tweet_query = 'INSERT INTO "MentionedInTweet" (uid, tid) VALUES (%s, %s);' % (uid, tid)
                    cursor.execute(user_mentioned_in_tweet_query)
                    cnx.commit()

            #insert links
            if len(link_matches) > 0:
                for match in link_matches:
                    link_exists_query = 'SELECT lid FROM "Link" WHERE linkurl = \'%s\';' % match
                    cursor.execute(link_exists_query)
                    rows = cursor.fetchall()
                    if len(rows) == 0:
                        link_query = 'INSERT INTO "Link" (linkurl) VALUES (\'%s\');' % match
                        cursor.execute(link_query)
                        cnx.commit()
                        get_link_id_query = 'SELECT lid FROM "Link" WHERE linkurl = \'%s\';' % match
                        cursor.execute(get_link_id_query);
                        rows = cursor.fetchall()
                    lid = rows[0][0]
                    link_in_tweet_query = 'INSERT INTO "LinkInTweet" (lid, tid) VALUES (%s, %s);' % (
                    lid, tid)
                    cursor.execute(link_in_tweet_query)
                    cnx.commit()
            print "Successfully imported %d records" % index
    os.remove(project_location + '02/cleaned_data.csv')
    cursor.close()
    cnx.close()


if __name__ == "__main__":
    init()
    select_useful_columns()
    clean_data()
    import_data()
