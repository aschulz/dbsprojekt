<html>
<head>
<title>Hashtags Visualisierung</title>
</head>
<body>
<!-- START SIGMA IMPORTS -->
<script src="sigma.js-1.2.0/src/sigma.core.js"></script>
<script src="sigma.js-1.2.0/src/conrad.js"></script>
<script src="sigma.js-1.2.0/src/utils/sigma.utils.js"></script>
<script src="sigma.js-1.2.0/src/utils/sigma.polyfills.js"></script>
<script src="sigma.js-1.2.0/src/sigma.settings.js"></script>
<script src="sigma.js-1.2.0/src/classes/sigma.classes.dispatcher.js"></script>
<script src="sigma.js-1.2.0/src/classes/sigma.classes.configurable.js"></script>
<script src="sigma.js-1.2.0/src/classes/sigma.classes.graph.js"></script>
<script src="sigma.js-1.2.0/src/classes/sigma.classes.camera.js"></script>
<script src="sigma.js-1.2.0/src/classes/sigma.classes.quad.js"></script>
<script src="sigma.js-1.2.0/src/classes/sigma.classes.edgequad.js"></script>
<script src="sigma.js-1.2.0/src/captors/sigma.captors.mouse.js"></script>
<script src="sigma.js-1.2.0/src/captors/sigma.captors.touch.js"></script>
<script src="sigma.js-1.2.0/src/renderers/sigma.renderers.canvas.js"></script>
<script src="sigma.js-1.2.0/src/renderers/sigma.renderers.webgl.js"></script>
<script src="sigma.js-1.2.0/src/renderers/sigma.renderers.svg.js"></script>
<script src="sigma.js-1.2.0/src/renderers/sigma.renderers.def.js"></script>
<script src="sigma.js-1.2.0/src/renderers/webgl/sigma.webgl.nodes.def.js"></script>
<script src="sigma.js-1.2.0/src/renderers/webgl/sigma.webgl.nodes.fast.js"></script>
<script src="sigma.js-1.2.0/src/renderers/webgl/sigma.webgl.edges.def.js"></script>
<script src="sigma.js-1.2.0/src/renderers/webgl/sigma.webgl.edges.fast.js"></script>
<script src="sigma.js-1.2.0/src/renderers/webgl/sigma.webgl.edges.arrow.js"></script>
<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.labels.def.js"></script>
<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.hovers.def.js"></script>
<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.nodes.def.js"></script>
<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.edges.def.js"></script>
<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.edges.curve.js"></script>
<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.edges.arrow.js"></script>
<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.edges.curvedArrow.js"></script>
<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.edgehovers.def.js"></script>
<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.edgehovers.curve.js"></script>
<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.edgehovers.arrow.js"></script>
<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.edgehovers.curvedArrow.js"></script>
<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.extremities.def.js"></script>
<script src="sigma.js-1.2.0/src/renderers/svg/sigma.svg.utils.js"></script>
<script src="sigma.js-1.2.0/src/renderers/svg/sigma.svg.nodes.def.js"></script>
<script src="sigma.js-1.2.0/src/renderers/svg/sigma.svg.edges.def.js"></script>
<script src="sigma.js-1.2.0/src/renderers/svg/sigma.svg.edges.curve.js"></script>
<script src="sigma.js-1.2.0/src/renderers/svg/sigma.svg.labels.def.js"></script>
<script src="sigma.js-1.2.0/src/renderers/svg/sigma.svg.hovers.def.js"></script>
<script src="sigma.js-1.2.0/src/middlewares/sigma.middlewares.rescale.js"></script>
<script src="sigma.js-1.2.0/src/middlewares/sigma.middlewares.copy.js"></script>
<script src="sigma.js-1.2.0/src/misc/sigma.misc.animation.js"></script>
<script src="sigma.js-1.2.0/src/misc/sigma.misc.bindEvents.js"></script>
<script src="sigma.js-1.2.0/src/misc/sigma.misc.bindDOMEvents.js"></script>
<script src="sigma.js-1.2.0/src/misc/sigma.misc.drawHovers.js"></script>
<!-- END SIGMA IMPORTS -->
<div id="container">
  <style>
    #graph-container {
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      position: absolute;
    }
  </style>
  <div id="graph-container"></div>
</div>
<?php
$conn_string = "host=localhost port=5433 dbname=Election user=localuser password=local";

$conn = pg_pconnect($conn_string);


if (!$conn) {
  echo "Ein Fehler ist aufgetreten.1\n";
  exit;
}
$result = pg_query($conn, "SELECT DISTINCT COUNT(*) FROM \"Clustercenter\"");
if (!$result) {
  echo "Ein Fehler ist aufgetreten.2\n";
  exit;
}else $countClustercenter = pg_fetch_row($result)[0];

$result = pg_query($conn, "SELECT COUNT(*) FROM \"Coordinate\"");
if (!$result) {
  echo "Ein Fehler ist aufgetreten.2\n";
  exit;
}
$countHashtags =pg_fetch_row($result)[0];

$result_array = Array();
/*while($row = pg_fetch_row($result)) {

     $result_array[] = "[".$row[0]."]";
}*/

 $json_temp_array = json_encode($result_array);
 $json_array = str_replace("\"","",$json_temp_array);


?>
<script>
/**
 * This is a basic example on how to instantiate sigma. A random graph is
 * generated and stored in the "graph" variable, and then sigma is instantiated
 * directly with the graph.
 *
 * The simple instance of sigma is enough to make it render the graph on the on
 * the screen, since the graph is given directly to the constructor.
 */
var i,
    s,
    o,
    N = <?php echo $countHashtags; ?>,
    E = 500,
    C = <?php echo $countClustercenter; ?>,
    cs = [],
    g = {
      nodes: [],
      edges: []
    };
<?php 
$resultCluster = pg_query($conn, "SELECT \"center_hashtag_id\" FROM \"Clustercenter\" ORDER BY \"cluster_id\" ASC");
while($row = pg_fetch_row($resultCluster)){
	echo "cs.push({id:";
	echo $row[0];?>
	,nodes: [],
        color: '#' + (
        Math.floor(Math.random() * 16777215).toString(16) + '000000'
    	).substr(0, 6)
  });
<?php	
}
$result = pg_query($conn, "SELECT h.hid, h.name,c.xcor,c.ycor,COUNT(t.hid=h.hid) FROM \"Coordinate\" AS c, \"Hashtag\" AS h, \"HashtagInTweet\" as t WHERE h.hid = c.hid AND h.hid=t.hid GROUP BY h.hid,h.name,c.xcor,c.ycor;");
if (!$result) {
  echo "Ein Fehler ist aufgetreten.\n";
  exit;
}
$nodeForEdges = Array();
$i = 0; //nodes counter
$j=0;//Edges Counter
while($row = pg_fetch_row($result)){

?>
o = cs[<?php echo $i; ?>];
g.nodes.push({
id: 'n' + '<?php echo $row[0]; ?>',
 label: '#' + '<?php echo $row[1]; ?>',
 x: <?php echo $row[2]; ?>,
 y: <?php echo $row[3]; ?>,
 size: <?php echo $row[4]; ?>,
 color: o.color
 });
 o.nodes.push('n' + '<?php echo $row[0]; ?>');
<?php
$nodeForEdges[$i]=$row[0];
$i++;
}
/*
$j=0;//Edges Counter
for ($i =0;$i<$countHashtags;$i++){
	$resultEdge = pg_query($conn, "SELECT DISTINCT h.\"hid\" FROM \"HashtagInTweet\" AS h WHERE h.hid != $nodeForEdges[$i] AND h.tid IN 		(SELECT t.tid FROM \"HashtagInTweet\" AS t WHERE t.hid=$nodeForEdges[$i]);");
	if (!$resultEdge) {
  		echo "Ein Fehler ist aufgetreten:\n";
 		exit;
	}
while($rowEdge = pg_fetch_row($resultEdge)){
?>
 g.edges.push({
    id: 'e' + <?php echo $j; ?>,
    source: 'n' + <?php echo $nodeForEdges[$i]; ?>,
    target: 'n' + <?php echo $rowEdge[0]; ?>,
    size: 1,
    color: '#ccc'
  });
<?php
$j++;
}
}*/
?>
/* Edge generator 
for (i = 0; i < E; i++)
  g.edges.push({
    id: 'e' + i,
    source: 'n' + (Math.random() * N | 0),
    target: 'n' + (Math.random() * N | 0),
    size: Math.random(),
    color: '#ccc'
  });
*/
// Instantiate sigma:
s = new sigma({
  graph: g,
  container: 'graph-container'
});
</script>
</body>
</html>
<?php
pg_close($conn);
?>
