<!DOCTYPE html>
<html>
 	<head>
  		<title>Hashtag-Visualisierung</title>
		<script src="https://code.jquery.com/jquery-3.2.1.js"></script>

<?php
	$hid = 1;
	if(isset($_GET["hid"]) && !empty($_GET["hid"])) {
		$hid = $_GET["hid"];
	}

	$conn_string = "host=localhost port=5432 dbname=Election user=carl password=carl";
	$conn = pg_pconnect($conn_string);
	if (!$conn) {
	  echo "Die Verbindung zur Datenbank konnte nicht hergestellt werden\n";
	  exit;
	}
	$resultSelectedHashtag = pg_query($conn, "SELECT hid, name FROM \"Hashtag\" WHERE hid=". $hid .";");
	$resultAllHashtags = pg_query($conn, "SELECT hid, name FROM \"Hashtag\";");
	$resultHashtagSum = pg_query($conn, "SELECT EXTRACT(DOY FROM t.tweettime), COUNT(*) FROM \"Tweet\" AS t, \"HashtagInTweet\" AS h WHERE t.tid = h.tid GROUP BY EXTRACT(DOY FROM t.tweettime) ORDER BY EXTRACT(DOY FROM t.tweettime) ASC;");
	$resultHashtagSingle = pg_query($conn, "SELECT EXTRACT(MONTH FROM t.tweettime), COUNT(*) FROM \"Tweet\" AS t, \"HashtagInTweet\" AS h WHERE t.tid = h.tid AND h.hid=" . $hid . " GROUP BY EXTRACT(MONTH FROM t.tweettime) ORDER BY EXTRACT(MONTH FROM t.tweettime) ASC;");
	if (!$resultHashtagSum || !$resultHashtagSingle || !$resultAllHashtags || !$resultSelectedHashtag) {
	  echo "Bei der Datenbankabfrage ist ein Fehler aufgetreten.\n";
	  exit;
	}
	$row = pg_fetch_row($resultSelectedHashtag, 0);
	$selected_hashtag = $row[1];
	$resultHashtagSumArray = array();
	$resultHashtagSingleArray = array();
	$months = array(
		1 => "Januar",
		2 => "Februar",
		3 => "Maerz",
		4 => "April",
		5 => "Mai",
		6 => "Juni",
		7 => "Juli",
		8 => "August",
		9 => "September",
		10 => "Oktober",
		11 => "November",
		12 => "Dezember"
	);

	for($i=1; $i<=271; $i++) {
		$date_format_string = "2016 " . $i;
		$date = DateTime::createFromFormat( 'Y z' , $date_format_string);
		$date->modify("-1 day");
		$date_string = date_format($date, "'d.m.'");
	    $resultHashtagSumArray[$i] = "[".$date_string.", 0]";
	}

	while($row = pg_fetch_row($resultHashtagSum)) {
		$date_format_string = "2016 " . $row[0];
		$date = DateTime::createFromFormat( 'Y z' , $date_format_string);
		$date->modify("-1 day");
		$date_string = date_format($date, "'d.m.'");
	    $resultHashtagSumArray[$row[0]] = "[".$date_string.",".$row[1]."]";
	}
	$resultHashtagSumArray = array_values($resultHashtagSumArray);

	$rows_len = pg_num_rows($resultHashtagSingle);
	$j = 1;
	for($i=0; $i<$rows_len; $i++) {
		$row = pg_fetch_row($resultHashtagSingle, $i);
		for($k=$j; $k<=9; $k++) {
			if ($k < $row[0]) {
				$date_string = "'" . $months[$k] . " 2016'";
				array_push($resultHashtagSingleArray, "[".$date_string.",0]");
				$j++;
			}
			else {
				$date_string = "'" . $months[$row[0]] . " 2016'";
			    array_push($resultHashtagSingleArray, "[".$date_string.",".$row[1]."]");
				$j++;
				break;
			}
		}
	}
	for($i=$j; $i<=9; $i++) {
		$date_string = "'" . $months[$i] . " 2016'";
		array_push($resultHashtagSingleArray, "[".$date_string.",0]");
	}

	$json_temp_array = json_encode($resultHashtagSumArray);
	$json_array_hashtag_sum = str_replace("\"","",$json_temp_array);

	$json_temp_array = json_encode($resultHashtagSingleArray);
	$json_array_hashtag_single = str_replace("\"","",$json_temp_array);

	pg_close($conn);
?>

		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script>
		   	google.load('visualization', '1', {packages: ['corechart', 'bar']});
			var dataHashtagSum = <?php echo $json_array_hashtag_sum; ?> ;
			var dataHashtagSingle = <?php echo $json_array_hashtag_single; ?> ;
			google.setOnLoadCallback(function() {drawBarchartHashtagSum(dataHashtagSum);});
	        google.setOnLoadCallback(function() {drawBarchartHashtagSingle(dataHashtagSingle);});

			function drawBarchartHashtagSum(dataHashtagSum) {
				var data = new google.visualization.DataTable();
	            data.addColumn('string', 'Tag');
	            data.addColumn('number', 'Auftreten von Hashtags');
	            data.addRows(dataHashtagSum);
				var options = {
	                chartArea: {top: 20, left: 60, width: '95%'},
	                vAxis: { format:'#', minValue: 0, viewWindow: {min:0}},
	                colors:['#95c11f',],
					legend: 'none'
	            };
				var chart = new google.visualization.ColumnChart(
                		document.getElementById('barchartHashtagSum'));
	            chart.draw(data, options);
			}

			function drawBarchartHashtagSingle(dataHashtagSum) {
				var data = new google.visualization.DataTable();
	            data.addColumn('string', 'Tag');
	            data.addColumn('number', 'Auftreten des Hashtags');
	            data.addRows(dataHashtagSingle);
				var options = {
	                chartArea: {top: 20, left: 60, width: '95%'},
	                vAxis: { format:'#', minValue: 0, viewWindow: {min:0}},
	                colors:['#0078bf',],
					legend: 'none'
	            };
				var chart = new google.visualization.ColumnChart(
	                    document.getElementById('barchartHashtagSingle'));
	            chart.draw(data, options);
			}
	    </script>
	</head>
	<body>
		<h3>Verlauf aller Hashtags</h3>
		<div id="barchartHashtagSum"></div>
		<h3>Verlauf eines Hashtags: #<?php echo $selected_hashtag; ?></h3>
		<form action="/visualization.php" id="hashtag_form" method="GET">
			<select name="hid" form="hashtag_form">
				<?php
					while($row = pg_fetch_row($resultAllHashtags)) {
						if ($row[0] == $hid) {
							echo "<option selected=\"selected\" value=\"". $row[0] ."\">#". $row[1] ."</option>\n";
						}
						else {
							echo "<option value=\"". $row[0] ."\">#". $row[1] ."</option>\n";
						}
					}
				?>
			</select>
  			<input type="submit" value="Hashtag ausw&auml;hlen">
		</form>
	    <div id="barchartHashtagSingle"></div>
	</body>
</html>
