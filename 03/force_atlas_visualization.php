<!DOCTYPE html>
<html>
<head>
	<title>Visualisierung des Hashtagnetzwerks</title>

	<script src="sigma.js-1.2.0/src/sigma.core.js"></script>
	<script src="sigma.js-1.2.0/src/conrad.js"></script>
	<script src="sigma.js-1.2.0/src/utils/sigma.utils.js"></script>
	<script src="sigma.js-1.2.0/src/utils/sigma.polyfills.js"></script>
	<script src="sigma.js-1.2.0/src/sigma.settings.js"></script>
	<script src="sigma.js-1.2.0/src/classes/sigma.classes.dispatcher.js"></script>
	<script src="sigma.js-1.2.0/src/classes/sigma.classes.configurable.js"></script>
	<script src="sigma.js-1.2.0/src/classes/sigma.classes.graph.js"></script>
	<script src="sigma.js-1.2.0/src/classes/sigma.classes.camera.js"></script>
	<script src="sigma.js-1.2.0/src/classes/sigma.classes.quad.js"></script>
	<script src="sigma.js-1.2.0/src/classes/sigma.classes.edgequad.js"></script>
	<script src="sigma.js-1.2.0/src/captors/sigma.captors.mouse.js"></script>
	<script src="sigma.js-1.2.0/src/captors/sigma.captors.touch.js"></script>
	<script src="sigma.js-1.2.0/src/renderers/sigma.renderers.canvas.js"></script>
	<script src="sigma.js-1.2.0/src/renderers/sigma.renderers.webgl.js"></script>
	<script src="sigma.js-1.2.0/src/renderers/sigma.renderers.svg.js"></script>
	<script src="sigma.js-1.2.0/src/renderers/sigma.renderers.def.js"></script>
	<script src="sigma.js-1.2.0/src/renderers/webgl/sigma.webgl.nodes.def.js"></script>
	<script src="sigma.js-1.2.0/src/renderers/webgl/sigma.webgl.nodes.fast.js"></script>
	<script src="sigma.js-1.2.0/src/renderers/webgl/sigma.webgl.edges.def.js"></script>
	<script src="sigma.js-1.2.0/src/renderers/webgl/sigma.webgl.edges.fast.js"></script>
	<script src="sigma.js-1.2.0/src/renderers/webgl/sigma.webgl.edges.arrow.js"></script>
	<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.labels.def.js"></script>
	<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.hovers.def.js"></script>
	<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.nodes.def.js"></script>
	<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.edges.def.js"></script>
	<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.edges.curve.js"></script>
	<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.edges.arrow.js"></script>
	<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.edges.curvedArrow.js"></script>
	<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.edgehovers.def.js"></script>
	<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.edgehovers.curve.js"></script>
	<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.edgehovers.arrow.js"></script>
	<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.edgehovers.curvedArrow.js"></script>
	<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.extremities.def.js"></script>
	<script src="sigma.js-1.2.0/src/renderers/svg/sigma.svg.utils.js"></script>
	<script src="sigma.js-1.2.0/src/renderers/svg/sigma.svg.nodes.def.js"></script>
	<script src="sigma.js-1.2.0/src/renderers/svg/sigma.svg.edges.def.js"></script>
	<script src="sigma.js-1.2.0/src/renderers/svg/sigma.svg.edges.curve.js"></script>
	<script src="sigma.js-1.2.0/src/renderers/svg/sigma.svg.labels.def.js"></script>
	<script src="sigma.js-1.2.0/src/renderers/svg/sigma.svg.hovers.def.js"></script>
	<script src="sigma.js-1.2.0/src/middlewares/sigma.middlewares.rescale.js"></script>
	<script src="sigma.js-1.2.0/src/middlewares/sigma.middlewares.copy.js"></script>
	<script src="sigma.js-1.2.0/src/misc/sigma.misc.animation.js"></script>
	<script src="sigma.js-1.2.0/src/misc/sigma.misc.bindEvents.js"></script>
	<script src="sigma.js-1.2.0/src/misc/sigma.misc.bindDOMEvents.js"></script>
	<script src="sigma.js-1.2.0/src/misc/sigma.misc.drawHovers.js"></script>
	<script src="sigma.js-1.2.0/plugins/sigma.layout.forceAtlas2/worker.js"></script>
	<script src="sigma.js-1.2.0/plugins/sigma.layout.forceAtlas2/supervisor.js"></script>
	<script src="sigma.js-1.2.0/plugins/sigma.renderers.customShapes/shape-library.js"></script>
	<script src="sigma.js-1.2.0/plugins/sigma.renderers.customShapes/sigma.renderers.customShapes.js"></script>

	<style>
		body {
			background-color: #FFF;
		}

		#graph-container {
			top: 0;
			bottom: 0;
			left: 0;
			right: 0;
			position: absolute;
		}
    </style>
</head>
<body>
	<h3>Visualisierung des Hashtagnetzwerks</h3>
	<div id="container">
		<div id="graph-container"></div>
	</div>

<?php
ini_set('display_errors', 'On');
$conn_string = "host=localhost port=5432 dbname=Election user=carl password=carl";
$conn = pg_pconnect($conn_string);
if (!$conn) {
  echo "Ein Fehler ist aufgetreten.1\n";
  exit;
}

$get_cluster_centers = pg_query($conn, "SELECT cluster_id, center_hashtag_id FROM \"Clustercenter\" ORDER BY cluster_id;");
if (!$get_cluster_centers) {
  echo "Fehler bei der Datenbankabfrage 1\n";
  exit;
}
$cluster_centers = array();
while ($row = pg_fetch_row($get_cluster_centers)) {
	$cluster_centers["$row[0]"] = $row[1];
}
$count_cluster_centers = sizeof($cluster_centers);

$get_hashtag_names = pg_query($conn, "SELECT hid, name FROM \"Hashtag\";");
if (!$get_hashtag_names) {
  echo "Fehler bei der Datenbankabfrage 2\n";
  exit;
}
$hashtag_names = array();
while ($row = pg_fetch_row($get_hashtag_names)) {
	$hashtag_names["$row[0]"] = $row[1];
}
$count_hashtags = sizeof($hashtag_names);

$get_hashtag_clusters = pg_query($conn, "SELECT hid, cluster_id FROM \"Cluster\";");
if (!$get_hashtag_clusters) {
  echo "Fehler bei der Datenbankabfrage 3\n";
  exit;
}
$hashtag_clusters = array();
while ($row = pg_fetch_row($get_hashtag_clusters)) {
	$hashtag_clusters["$row[0]"] = $row[1];
}

$get_hashtag_count = pg_query($conn, "SELECT H.hid, COUNT(*) FROM \"HashtagInTweet\" AS H, \"Tweet\" AS T WHERE H.tid = T.tid GROUP BY H.hid ORDER BY H.hid ASC;");
if (!$get_hashtag_count) {
  echo "Fehler bei der Datenbankabfrage 4\n";
  exit;
}
$hashtag_count = array();
while ($row = pg_fetch_row($get_hashtag_count)) {
	$hashtag_count["$row[0]"] = $row[1];
}

$get_distances = pg_query($conn, "SELECT hid1, hid2, distance FROM \"Distance\" ORDER BY hid1, hid2;");
if (!$get_distances) {
  echo "Fehler bei der Datenbankabfrage 5\n";
  exit;
}

$get_hashtags_in_same_tweet = pg_query($conn, "SELECT MIN(H.hid), MIN(HH.hid) FROM \"HashtagInTweet\" AS H, \"HashtagInTweet\" AS HH WHERE H.tid = HH.tid AND H.hid <> HH.hid GROUP BY (H.hid, HH.hid) ORDER BY H.hid, HH.hid;");
if (!$get_hashtags_in_same_tweet) {
  echo "Fehler bei der Datenbankabfrage 6\n";
  exit;
}
?>
	<script type="text/javascript">
		sigma.utils.pkg('sigma.canvas.nodes');

		var s,
		o,
	    cs = [],
		N = <?php echo $count_hashtags; ?>,
	    g = {
	      nodes: [],
	      edges: []
	    };

		<?php foreach($cluster_centers as $cluster_id => $cluster_center_hashtag): ?>
			cs.push({
				id: <?php echo $cluster_id; ?>,
				nodes: [],
				color: '#' + (
				  Math.floor(Math.random() * 16777215).toString(16) + '000000'
				).substr(0, 6)
			});
		<?php endforeach; ?>

		<?php foreach($hashtag_names as $hashtag_id => $hashtag_name): ?>
			o = cs[<?php echo $hashtag_clusters[$hashtag_id]; ?>];
			var node = {
				id: 'n' + <?php echo $hashtag_id; ?>,
				label: '#' + '<?php echo $hashtag_name ?>',
				x: 100 * Math.cos(2 * Math.PI / N),
				y: 100 * Math.sin(2 * Math.PI / N),
				size: <?php echo 2+intval(sqrt($hashtag_count[$hashtag_id])); ?>,
				color: o.color,
				<?php if(in_array($hashtag_id, array_values($cluster_centers))) { echo 'type: "square",'; } ?>
			};
			g.nodes.push(node);
			o.nodes.push('n' + <?php echo $hashtag_id; ?>);
		<?php endforeach; ?>

		<?php $edges_counter = array(); ?>
		<?php $e = 1; ?>
		<?php while($row = pg_fetch_row($get_distances)): ?>
			<?php $distance = intval($row[2]); ?>
			<?php if ($row[0] != $row[1]): ?>
				<?php for ($i = 0; $i < (20 - $distance); $i++): ?>
					g.edges.push({
						id: 'e' + <?php echo $e; ?>,
						source: 'n' + <?php echo $row[0]; ?>,
						target: 'n' + <?php echo $row[1]; ?>,
						count: <?php echo $i+1; ?>,
						color: '#FFF',
					});
					<?php $edges_counter["(".$row[0].",".$row[1].")"] = $i+1; ?>
					<?php $e++; ?>
				<?php endfor; ?>
			<?php endif; ?>
		<?php endwhile; ?>

		<?php while($row = pg_fetch_row($get_hashtags_in_same_tweet)): ?>
			<?php $min = min($row[0], $row[1]); ?>
			<?php $max = max($row[0], $row[1]); ?>
			g.edges.push({
				id: 'e' + <?php echo $e; ?>,
				source: 'n' + <?php echo $row[0]; ?>,
				target: 'n' + <?php echo $row[1]; ?>,
				count: <?php echo $edges_counter["(".$min.",".$max.")"] + 1; ?>,
				color: '#CCC',
			});
			<?php $e++; ?>
		<?php endwhile; ?>

		s = new sigma({
			graph: g,
			renderer: {
		      container: document.getElementById('graph-container'),
		      type: 'canvas'
		    },
		});
		CustomShapes.init(s);
		s.refresh();

		s.startForceAtlas2({worker: true, barnesHutOptimize: false, linLogMode: false});
	</script>
</body>
</html>
