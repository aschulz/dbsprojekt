<!DOCTYPE html>
<html>
 <head>
  <title>Verlauf der Apple-Aktie</title>
  <style type="text/css">   
   #barchart{width:1024px; height:400px;}  
    #barchartHashtag{width:1024px; height:400px;} 
  </style>
  <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/flot/0.8.3/jquery.flot.js"></script>
<?php
$conn_string = "host=localhost port=5433 dbname=Election user=localuser password=local";

$conn = pg_pconnect($conn_string);


if (!$conn) {
  echo "Ein Fehler ist aufgetreten.1\n";
  exit;
}

$result = pg_query($conn, "SELECT EXTRACT('DOY' FROM t.\"tweettime\"), COUNT(*) FROM \"Tweet\" AS t GROUP BY EXTRACT('DOY' FROM t.\"tweettime\") ORDER BY EXTRACT('DOY' FROM t.\"tweettime\") ASC;");
$resultHashtag = pg_query($conn, "SELECT EXTRACT('DOY' FROM t.\"tweettime\"), COUNT(*) FROM \"Tweet\" AS t,\"HashtagInTweet\" AS h WHERE t.\"tid\" = h.\"tid\" AND h.\"hid\"='9' GROUP BY EXTRACT('DOY' FROM t.\"tweettime\") ORDER BY EXTRACT('DOY' FROM t.\"tweettime\") ASC;
");
if (!$result || !$resultHashtag) {
  echo "Ein Fehler ist aufgetreten.\n";
  exit;
}
$result_hashtag_array = Array();
$result_array = Array();
    while($row = pg_fetch_row($result)) {
        $result_array[] = "[".$row[0].",".$row[1]."]";
    }
    while($row = pg_fetch_row($resultHashtag)) {
        $result_hashtag_array[] = "[".$row[0].",".$row[1]."]";
    }
	

 $json_temp_array = json_encode($result_array);
 $json_array = str_replace("\"","",$json_temp_array);

 $json_temp_array = json_encode($result_hashtag_array);
 $json_hashtag_array = str_replace("\"","",$json_temp_array);

pg_close($conn);
?>
<script type="text/javascript">
	var data = <?php echo $json_array; ?> ;
	var dataHashtag = <?php echo $json_hashtag_array; ?> ;
        var dataset = [{color: '#0000FF', label: "Tweets pro Tag", data: data}];
        var datasetHashtag = [{color: '#FF0000', label: "americaFirst", data: dataHashtag}];
	var optionsB = {
			    legend: {position: "nw"},
			    series: {bars: {show: true}},
			    bars: {align: "center", barWidth: 0.5}
			};
        var optionsBH = {
			    legend: {position: "nw"},
			    series: {bars: {show: true}},
			    bars: {align: "center", barWidth: 0.5}
			};
	
	
        $(document).ready(function () {
	    $.plot($("#barchart"), dataset, optionsB);	
	    $.plot($("#barchartHashtag"), datasetHashtag, optionsBH);		
        });
    </script>
</head>
<body>
   <div id="barchart"></div>
    <div id="barchartHashtag"></div>
</body>
</html>


 
