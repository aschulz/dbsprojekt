#! usr/bin/python
# -*- coding: utf-8 -*-

import random
import psycopg2
import sys

reload(sys)
sys.setdefaultencoding('utf8')

db_host = "localhost"
db_port = "5432"
db_user = "carl"
db_password = "carl"

class Clustering(object):

    def __init__(self):
        self.k = 16
        self.max_iterations = 10
        self.weightings = {
            'same_tweet': 10.0,
            'same_hour': 3.0,
            'same_day': 2.0,
            'same_3days': 1.0,
            'same_author': 4.0
        }
        self.sum_weightings = sum(self.weightings.values())
        self.cluster_centers = []
        self.cnx = psycopg2.connect(user=db_user,
                                    host=db_host,
                                    database="Election",
                                    port=db_port,
                                    password=db_password)
        self.cursor = self.cnx.cursor()
        self.all_hashtag_ids = []
        self.distances = []
        self.cluster_members = {}
        self.max_occurences_within_one_hour = []
        self.max_occurences_within_one_day = []
        self.max_occurences_within_three_days = []


    def get_all_hashtags(self):
        all_hashtag_ids_query = 'SELECT hid FROM "Hashtag";'
        self.cursor.execute(all_hashtag_ids_query)
        result = self.cursor.fetchall()
        self.all_hashtag_ids = [x[0] for x in result]
        self.all_hashtag_ids.sort()
        for id in self.all_hashtag_ids:
            self.cluster_members[id] = None


    def get_random_cluster_start_centers(self):
        for i in range(self.k):
            rand_int = random.randint(0, len(self.all_hashtag_ids)-1)
            max_distance = reduce(lambda a,b: a and b, [self.dist(self.all_hashtag_ids[rand_int], x) == self.sum_weightings for x in self.cluster_centers], True)
            is_in_already = self.all_hashtag_ids[rand_int] in self.cluster_centers
            while is_in_already or not max_distance:
                rand_int = random.randint(0, len(self.all_hashtag_ids) - 1)
                max_distance = [self.dist(self.all_hashtag_ids[rand_int], x) for x in self.cluster_centers]
                is_in_already = self.all_hashtag_ids[rand_int] in self.cluster_centers
            self.cluster_centers.append(self.all_hashtag_ids[rand_int])


    def calculate_max_time_interval_occurences(self):
        self.max_occurences_within_one_hour = 155
        # print "Berechne maximales Auftreten von 2 Hashtags in 1h..."
        # self.max_occurences_within_one_hour = max(
        #     [self.get_occurences_within_time_interval(i, j, '01:00:00') for i in self.all_hashtag_ids for j in self.all_hashtag_ids if
        #      i < j])
        self.max_occurences_within_one_day = 836
        # print "Berechne maximales Auftreten von 2 Hashtags in 1 Tag..."
        # self.max_occurences_within_one_day = max(
        #     [self.get_occurences_within_time_interval(i, j, '23:59:59') for i in self.all_hashtag_ids for j in self.all_hashtag_ids if
        #      i < j])
        self.max_occurences_within_three_days = 1992
        # print "Berechne maximales Auftreten von 2 Hashtags in 3 Tagen..."
        # self.max_occurences_within_three_days = max(
        #     [self.get_occurences_within_time_interval(i, j, '3 days') for i in self.all_hashtag_ids for j in self.all_hashtag_ids if
        #      i < j])


    def get_occurences_within_time_interval(self, hid1, hid2, interval):
        occurences_within_time_interval_query = 'WITH ' \
                                                'tweets1 AS (SELECT * FROM "Tweet" AS T, "HashtagInTweet" AS H WHERE H.tid = T.tid AND  H.hid={0}),' \
                                                'tweets2 AS (SELECT * FROM "Tweet" AS T, "HashtagInTweet" AS H WHERE H.tid = T.tid AND H.hid={1})' \
                                                'SELECT COUNT(*) FROM tweets1 AS A, tweets2 AS B WHERE ' \
                                                'A.tweettime - B.tweettime >= interval \'-{2}\' AND A.tweettime - B.tweettime <= interval \'{2}\';'.format(
            hid1, hid2, interval)
        self.cursor.execute(occurences_within_time_interval_query)
        occurences_within_time_interval = self.cursor.fetchall()
        return occurences_within_time_interval[0][0]


    def get_occurences_in_same_tweet(self, hid1, hid2):
        occurences_in_same_tweet_query = 'WITH ' \
                                         'temp AS (SELECT array_agg(hid) AS a FROM "HashtagInTweet" GROUP BY tid) ' \
                                         'SELECT COUNT(*) FROM temp WHERE a @> ARRAY[{0},{1}];'.format(hid1, hid2)
        self.cursor.execute(occurences_in_same_tweet_query)
        occurences_in_same_tweet = self.cursor.fetchall()
        return occurences_in_same_tweet[0][0]


    def get_same_author_value(self, hid1, hid2):
        # get_clinton_uid_query = 'SELECT uid FROM "User" WHERE username = \'hillaryclinton\''
        # self.cursor.execute(get_clinton_uid_query)
        # clinton_id = self.cursor.fetchall()[0][0]
        # get_trump_uid_query = 'SELECT uid FROM "User" WHERE username = \'realdonaldtrump\''
        # self.cursor.execute(get_trump_uid_query)
        # trump_id = self.cursor.fetchall()[0][0]
        clinton_id = 1
        trump_id = 4
        count_query = 'WITH ' \
                      'count_in_tweets AS(SELECT T.uid AS uid, COUNT(*) AS count FROM "Tweet" AS T, "HashtagInTweet" AS H WHERE T.tid = H.tid AND T.uid IN({1},{2}) AND H.hid = {0} GROUP BY T.uid), ' \
                      'count_in_retweets AS(SELECT R.uid AS uid, COUNT(*) AS count FROM "Tweet" AS T, "HashtagInTweet" AS H, "HasRetweeted" AS R WHERE R.tid = H.tid AND R.tid = T.tid AND R.uid IN({1},{2}) AND H.hid = {0} GROUP BY R.uid) ' \
                      'SELECT uid, sum(count) FROM ' \
                      '(SELECT * FROM count_in_tweets ' \
                      'UNION ALL ' \
                      'SELECT * FROM count_in_retweets) ' \
                      'AS unioned GROUP BY uid;'
        self.cursor.execute(count_query.format(hid1, clinton_id, trump_id))
        counts_hid1 = self.cursor.fetchall()
        self.cursor.execute(count_query.format(hid2, clinton_id, trump_id))
        counts_hid2 = self.cursor.fetchall()
        counts_hid1_dict = {}
        counts_hid2_dict = {}
        for row in counts_hid1:
            counts_hid1_dict[row[0]] = float(row[1])
        if clinton_id not in counts_hid1_dict.keys():
            counts_hid1_dict[clinton_id] = 0
        if trump_id not in counts_hid1_dict.keys():
            counts_hid1_dict[trump_id] = 0
        for row in counts_hid2:
            counts_hid2_dict[row[0]] = float(row[1])
        if clinton_id not in counts_hid2_dict.keys():
            counts_hid2_dict[clinton_id] = 0
        if trump_id not in counts_hid2_dict.keys():
            counts_hid2_dict[trump_id] = 0
        top_tweeter_hid1 = max(counts_hid1_dict.iterkeys(), key=lambda k: counts_hid1_dict[k])
        top_tweeter_hid2 = max(counts_hid2_dict.iterkeys(), key=lambda k: counts_hid2_dict[k])
        if top_tweeter_hid1 != top_tweeter_hid2:
            return 0.0
        else:
            return 0.5 * (float(counts_hid1_dict[top_tweeter_hid1]) / float(sum(counts_hid1_dict.values())) + float(counts_hid2_dict[top_tweeter_hid1]) / float(sum(counts_hid2_dict.values())))


    def dist(self, hid1, hid2):
        if hid1 == hid2:
            return 0.0
        occurences_in_same_tweet = self.get_occurences_in_same_tweet(hid1, hid2)
        occurences_hid1_query = 'SELECT COUNT(*) FROM "HashtagInTweet" WHERE hid={0}'.format(hid1)
        occurences_hid2_query = 'SELECT COUNT(*) FROM "HashtagInTweet" WHERE hid={0}'.format(hid2)
        self.cursor.execute(occurences_hid1_query)
        occurences_hid1 = self.cursor.fetchall()
        self.cursor.execute(occurences_hid2_query)
        occurences_hid2 = self.cursor.fetchall()
        min_occurences = min(occurences_hid1, occurences_hid2)[0][0]
        occurences_within_one_hour = self.get_occurences_within_time_interval(hid1, hid2, '01:00:00')
        occurences_within_one_day = self.get_occurences_within_time_interval(hid1, hid2, '23:59:00')
        occurences_within_three_days = self.get_occurences_within_time_interval(hid1, hid2, '3 days')
        same_author_value = self.get_same_author_value(hid1, hid2)
        a = self.weightings['same_tweet'] * occurences_in_same_tweet / min_occurences
        b = self.weightings['same_hour'] * occurences_within_one_hour / self.max_occurences_within_one_hour
        c = self.weightings['same_day'] * occurences_within_one_day / self.max_occurences_within_one_day
        d = self.weightings['same_3days'] * occurences_within_three_days / self.max_occurences_within_three_days
        e = self.weightings['same_author'] * same_author_value
        return self.sum_weightings - a - b - c - d - e


    def calculate_all_distances(self):
        print "Berechne Distanzen zwischen allen Hashtags..."
        drop_table_query = 'DROP TABLE IF EXISTS "Distance" CASCADE;'
        self.cursor.execute(drop_table_query)
        self.cnx.commit()
        create_table_query = 'CREATE TABLE "Distance" (' \
                             'hid1 INTEGER NOT NULL,' \
                             'hid2 INTEGER NOT NULL,' \
                             'distance DOUBLE PRECISION NOT NULL' \
                             ');'
        self.cursor.execute(create_table_query)
        self.cnx.commit()
        for x in self.all_hashtag_ids:
            for y in self.all_hashtag_ids:
                if x <= y:
                    distance = self.dist(x, y)
                    insert_query = 'INSERT INTO "Distance" (hid1, hid2, distance) VALUES ({0},{1},{2});'.format(x, y,
                                                                                                               distance)
                    self.cursor.execute(insert_query)
                    print "Distanz für {0} und {1} berechnet: {2}".format(x, y, distance)
            self.cnx.commit()


    def get_distance(self, hid1, hid2):
        hid_1, hid_2 = min(hid1, hid2), max (hid1, hid2)
        query = 'SELECT distance FROM "Distance" WHERE hid1={0} AND hid2={1};'.format(hid_1, hid_2)
        self.cursor.execute(query)
        return self.cursor.fetchall()[0][0]


    def calculate_cluster_centers(self):
        print "Berechne Clusterzentren..."
        something_changed = False
        for cluster in range(self.k):
            members = [x for x in self.cluster_members.keys() if self.cluster_members.get(x) == cluster]
            members.sort()
            sums = {}
            for id in members:
                tuples = [(min(id, y), max(id, y)) for y in members]
                query = 'SELECT sum(distance) FROM "Distance" WHERE (hid1, hid2) IN ({0});'.format(str(tuples)[1:-1])
                self.cursor.execute(query)
                result = self.cursor.fetchall()[0][0]
                sums[id] = result
            old_center = self.cluster_centers[cluster]
            self.cluster_centers[cluster] = sums.keys()[sums.values().index(min(sums.values()))]
            if old_center == self.cluster_centers[cluster]:
                print "Zentrum von Cluster {0} bleibt {1}".format(cluster, old_center)
            else:
                print "Zentrum von Cluster {0} wechselt von {1} auf {2}".format(cluster, old_center, self.cluster_centers[cluster])
                something_changed = True
        return something_changed


    def cluster(self):
        print "Berechne Cluster..."
        for id in self.all_hashtag_ids:
            distances_to_calculate = [(min(id, cluster_center), max(id, cluster_center)) for cluster_center in self.cluster_centers]
            query = 'SELECT hid1, hid2, distance FROM "Distance" WHERE (hid1, hid2) IN ({0}) ORDER BY distance ASC LIMIT 1;'.format(str(distances_to_calculate)[1:-1])
            self.cursor.execute(query)
            result = self.cursor.fetchall()
            center_of_new_cluster = result[0][0] if result[0][1] == id else result[0][1]
            old_cluster = self.cluster_members[id]
            self.cluster_members[id] = self.cluster_centers.index(center_of_new_cluster)
            if old_cluster == self.cluster_members[id]:
                print "Hashtag {0} bleibt in Cluster {1}".format(id, old_cluster)
            else:
                print "Hashtag {0} wechselt von Cluster {1} in Cluster {2}".format(id, old_cluster, self.cluster_members[id])


    def kmeans(self):
        centers_changed = True
        i = 1
        while centers_changed and i <= self.max_iterations:
            print "Starte Iteration {0} von kmeans...".format(i)
            self.cluster()
            centers_changed = self.calculate_cluster_centers()
            i += 1
            print "aktuelle Clusterbelegung:"
            for j in range(self.k):
                print "Cluster {0} (Zentrum: {2}): {1}".format(j, [k for k,v in self.cluster_members.iteritems() if v == j], self.cluster_centers[j])
        print "Beende k-means nach {0} Iterationen.".format(i)


    def write_clusters_to_db(self):
        print "Schreibe Cluster in die Datenbank..."
        delete_query = 'DROP TABLE IF EXISTS "Cluster" CASCADE;'
        self.cursor.execute(delete_query)
        self.cnx.commit()
        create_query = 'CREATE TABLE "Cluster" (' \
                       'hid INTEGER NOT NULL REFERENCES "Hashtag", ' \
                       'cluster_id INTEGER NOT NULL' \
                       ');'
        self.cursor.execute(create_query)
        self.cnx.commit()
        insert_query = 'INSERT INTO "Cluster" (hid, cluster_id) VALUES {0};'.format(str([(k,v) for k,v in self.cluster_members.iteritems()])[1:-1])
        self.cursor.execute(insert_query)
        self.cnx.commit()
        delete_query2 = 'DROP TABLE IF EXISTS "Clustercenter" CASCADE;'
        self.cursor.execute(delete_query2)
        self.cnx.commit()
        create_query2 = 'CREATE TABLE "Clustercenter" (' \
                        'cluster_id INTEGER NOT NULL, ' \
                        'center_hashtag_id INTEGER NOT NULL REFERENCES "Hashtag" ' \
                        ');'
        self.cursor.execute(create_query2)
        self.cnx.commit()
        insert_query2 = 'INSERT INTO "Clustercenter" (cluster_id, center_hashtag_id) VALUES {0};'.format(
            str([(self.cluster_centers.index(cluster), cluster) for cluster in self.cluster_centers])[1:-1])
        self.cursor.execute(insert_query2)
        self.cnx.commit()


if __name__ == "__main__":
    cluster = Clustering()
    cluster.get_all_hashtags()
    cluster.calculate_max_time_interval_occurences()
    cluster.calculate_all_distances()
    cluster.get_random_cluster_start_centers()
    cluster.kmeans()
    cluster.write_clusters_to_db()
