<html>
<head>
<title>Visualisierung der Hashtags im US-Wahlkampf 2016</title>
</head>
<body>
<h1 style="font-family:Arial;font-size:16pt;">Hashtags im US-Wahlkampf 2016</h1>

<!-- START SIGMA IMPORTS -->
<script src="sigma.js-1.2.0/src/sigma.core.js"></script>
<script src="sigma.js-1.2.0/src/conrad.js"></script>
<script src="sigma.js-1.2.0/src/utils/sigma.utils.js"></script>
<script src="sigma.js-1.2.0/src/utils/sigma.polyfills.js"></script>
<script src="sigma.js-1.2.0/src/sigma.settings.js"></script>
<script src="sigma.js-1.2.0/src/classes/sigma.classes.dispatcher.js"></script>
<script src="sigma.js-1.2.0/src/classes/sigma.classes.configurable.js"></script>
<script src="sigma.js-1.2.0/src/classes/sigma.classes.graph.js"></script>
<script src="sigma.js-1.2.0/src/classes/sigma.classes.camera.js"></script>
<script src="sigma.js-1.2.0/src/classes/sigma.classes.quad.js"></script>
<script src="sigma.js-1.2.0/src/classes/sigma.classes.edgequad.js"></script>
<script src="sigma.js-1.2.0/src/captors/sigma.captors.mouse.js"></script>
<script src="sigma.js-1.2.0/src/captors/sigma.captors.touch.js"></script>
<script src="sigma.js-1.2.0/src/renderers/sigma.renderers.canvas.js"></script>
<script src="sigma.js-1.2.0/src/renderers/sigma.renderers.webgl.js"></script>
<script src="sigma.js-1.2.0/src/renderers/sigma.renderers.svg.js"></script>
<script src="sigma.js-1.2.0/src/renderers/sigma.renderers.def.js"></script>
<script src="sigma.js-1.2.0/src/renderers/webgl/sigma.webgl.nodes.def.js"></script>
<script src="sigma.js-1.2.0/src/renderers/webgl/sigma.webgl.nodes.fast.js"></script>
<script src="sigma.js-1.2.0/src/renderers/webgl/sigma.webgl.edges.def.js"></script>
<script src="sigma.js-1.2.0/src/renderers/webgl/sigma.webgl.edges.fast.js"></script>
<script src="sigma.js-1.2.0/src/renderers/webgl/sigma.webgl.edges.arrow.js"></script>
<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.labels.def.js"></script>
<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.hovers.def.js"></script>
<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.nodes.def.js"></script>
<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.edges.def.js"></script>
<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.edges.curve.js"></script>
<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.edges.arrow.js"></script>
<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.edges.curvedArrow.js"></script>
<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.edgehovers.def.js"></script>
<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.edgehovers.curve.js"></script>
<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.edgehovers.arrow.js"></script>
<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.edgehovers.curvedArrow.js"></script>
<script src="sigma.js-1.2.0/src/renderers/canvas/sigma.canvas.extremities.def.js"></script>
<script src="sigma.js-1.2.0/src/renderers/svg/sigma.svg.utils.js"></script>
<script src="sigma.js-1.2.0/src/renderers/svg/sigma.svg.nodes.def.js"></script>
<script src="sigma.js-1.2.0/src/renderers/svg/sigma.svg.edges.def.js"></script>
<script src="sigma.js-1.2.0/src/renderers/svg/sigma.svg.edges.curve.js"></script>
<script src="sigma.js-1.2.0/src/renderers/svg/sigma.svg.labels.def.js"></script>
<script src="sigma.js-1.2.0/src/renderers/svg/sigma.svg.hovers.def.js"></script>
<script src="sigma.js-1.2.0/src/middlewares/sigma.middlewares.rescale.js"></script>
<script src="sigma.js-1.2.0/src/middlewares/sigma.middlewares.copy.js"></script>
<script src="sigma.js-1.2.0/src/misc/sigma.misc.animation.js"></script>
<script src="sigma.js-1.2.0/src/misc/sigma.misc.bindEvents.js"></script>
<script src="sigma.js-1.2.0/src/misc/sigma.misc.bindDOMEvents.js"></script>
<script src="sigma.js-1.2.0/src/misc/sigma.misc.drawHovers.js"></script>
<!-- END SIGMA IMPORTS -->
<div id="container">
  <style>
    #graph-container {
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      position: absolute;
    }
  </style>
  <div id="graph-container"></div>
</div>
<?php
ini_set('display_errors', 'On');
$conn_string = "host=localhost port=5433 dbname=Election user=localuser password=local";

$conn = pg_pconnect($conn_string);


if (!$conn) {
  echo "Ein beim Verbindungsaufbau zur Datenbank ist aufgetreten.\n";
  exit;
}
$result = pg_query($conn, "SELECT DISTINCT COUNT(*) FROM \"Clustercenter\"");
if (!$result) {
  echo "Ein Fehler bei der SQL-Abfrage ist aufgetreten.\n";
  exit;
}else $countClustercenter = pg_fetch_row($result)[0];

$result = pg_query($conn, "SELECT COUNT(*) FROM \"Coordinate\"");
if (!$result) {
  echo "Ein Fehler bei der SQL-Abfrage ist aufgetreten.\n";
  exit;
}
$countHashtags =pg_fetch_row($result)[0];
?>
<script>
//Intialisierung der Variablen
var i,
    s,
    o,
    N = <?php echo $countHashtags; ?>,
    E,
    C = <?php echo $countClustercenter; ?>,
    cs = {},
    g = {
      nodes: [],
      edges: []
    };
<?php
//Erstellen der Cluster mit eigener Farben (abhängig von der ID)
$resultCluster = pg_query($conn, "SELECT \"center_hashtag_id\" FROM \"Clustercenter\" ORDER BY \"cluster_id\" ASC");
while($row = pg_fetch_row($resultCluster)){
	echo "cs[$row[0]]={id:"; 
	echo $row[0];?>
	,nodes: [],
        color: '#' + (
 	Math.floor(<?php echo pow((intval($row[0])>2)?intval($row[0]):rand(10,167)*16777215,16); ?>).toString(16) + '000000'
    	).substr(0, 6)
  };
<?php
}
//Auslesen der Daten für die Hashtags und Erstellen der Knoten im Graph
$result = pg_query($conn, "SELECT h.hid, h.name,c.xcor,c.ycor,c.clusterHashtag,COUNT(t.hid=h.hid) FROM \"Coordinate\" AS c, \"Hashtag\" AS h, \"HashtagInTweet\" as t WHERE h.hid = c.hid AND h.hid=t.hid GROUP BY h.hid,h.name,c.xcor,c.ycor,c.clusterHashtag ORDER BY h.hid ASC;");
if (!$result) {
  echo "Ein Fehler ist aufgetreten.\n";
  exit;
}
$nodeForEdges = Array();
$i = 0; //nodes counter
while($row = pg_fetch_row($result)){

?>
o = cs[<?php echo $row[4]; ?>];
g.nodes.push({
 id: 'n' + '<?php echo $row[0]; ?>',
 label: '#' + '<?php echo $row[1]; ?>',
 x: <?php echo $row[2]; ?>,
 y: <?php echo $row[3]; ?>,
 size: <?php echo 2+intval(sqrt($row[5])); ?>, //Die Größe des Knoten ist abhängig von der Anzahl des Tweets mit diesem Hashtag
 color: o.color //Farbe des Cluster
 });
 o.nodes.push('n' + <?php echo $row[0]; ?>);

<?php
// Die Hashtag-ID jedes Knoten im Graphen wird dem Array $nodeForEdges hinzugefügt
$nodeForEdges[$i]=$row[0];
$i++;
}
//Erstellen der Kanten im Graph für jeden Hashtag im Graph zu jedem anderen Hashtag, mit dem er gemeinsam in einem Tweet vorgekommen ist.
$j=0;
for ($i =0;$i<$countHashtags;$i++){
	$resultEdge = pg_query($conn, "SELECT DISTINCT h.\"hid\" FROM \"HashtagInTweet\" AS h WHERE h.hid != $nodeForEdges[$i] AND h.tid IN 		(SELECT t.tid FROM \"HashtagInTweet\" AS t WHERE t.hid=$nodeForEdges[$i]);");
	if (!$resultEdge) {
  		echo "Ein Fehler ist aufgetreten:\n";
 		exit;
	}
while($rowEdge = pg_fetch_row($resultEdge)){
?>
 g.edges.push({
    id: 'e' + <?php echo $j; ?>,
    source: 'n' + <?php echo $nodeForEdges[$i]; ?>,
    target: 'n' + <?php echo $rowEdge[0]; ?>,
    size: 1,
    color: '#ccc'
  });
<?php
$j++;
}
}
?>
//Erstellen des Graphen
s = new sigma({
  graph: g,
  container: 'graph-container'
});
</script>
</body>
</html>
<?php
pg_close($conn);
?>
