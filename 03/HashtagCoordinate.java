package database.client.hashtag.coordinates;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class HashtagCoordinate {
	public static int SCALE = 5;
	public static String DB_HOST="localhost";
	public static int DB_PORT=5433;
	public static String DB_NAME="Election";
	public static String DB_USER="localuser";
	public static String DB_PASS="local";

	public static void main(String[] args) {
		new HashtagCoordinate().run();
		
	}
	private Connection connection;
	private void run() {
		this.connection = this.connectDB();
		//Drop if exists and create Coordinate Database
		String dropTable = "DROP TABLE IF EXISTS \"Coordinate\"";
		String queryCreate = "CREATE TABLE IF NOT EXISTS \"Coordinate\" (hid INTEGER NOT NULL REFERENCES \"Hashtag\",xcor INTEGER,ycor INTEGER, clusterHashtag INTEGER)";
		ResultSet rsC;
		try {
			this.connection.createStatement().execute(dropTable);
			
			this.connection.createStatement().execute(queryCreate);
			System.out.println("Creating Table succeded");
			//Get all 16 centers of clusters and create coordinates for each of them	
			this.createClusterCoordinates();
			//Get all Cluster with coordinates and calls createCoordinatesForCluster method for each of them.
			String getClustercenterQ = "SELECT d.cluster_id,c.xcor,c.ycor,d.center_hashtag_id FROM \"Coordinate\" AS c, \"Clustercenter\" AS d WHERE d.center_hashtag_id=c.hid";
			rsC = this.connection.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE, 
					ResultSet.CONCUR_READ_ONLY).executeQuery(getClustercenterQ);
			while (rsC.next()) {
				this.createCoordinatesForCluster(rsC.getInt(1),rsC.getInt(2),rsC.getInt(3),rsC.getInt(4));
			}

			

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		
	}
	/**
	 * Creates the coordinates of all hashtags which are referenced to the given cluster and stores the coordinates
	 * in the Coordinate table of the Election database.
	 * @param clusterId id of the given cluster
	 * @param clusterX x coordinate of the given cluster
	 * @param clusterY y coordinate of the given cluster
	 * @param centerHashtagId hashtag id of the hashtag in the cluster center
	 * @throws SQLException
	 */
	private void createCoordinatesForCluster(int clusterId, int clusterX, int clusterY, int centerHashtagId) throws SQLException{
		System.out.println("Creating Hashtag Coordinates in Cluster"+clusterId);
		ResultSet rsCor;
		String queryCoorCluster = "WITH\n" + 
				"cluster_center AS (SELECT center_hashtag_id AS a FROM \"Clustercenter\"\n" + 
				"WHERE cluster_id = "+clusterId+"),\n" + 
				"cluster_members AS (SELECT hid AS b FROM \"Cluster\" WHERE cluster_id = "+clusterId+")\n" + 
				"SELECT CASE\n" + 
				"          WHEN D.hid1 IN (SELECT a FROM cluster_center) THEN D.hid2\n" + 
				"          WHEN D.hid2 IN (SELECT a FROM cluster_center) THEN D.hid1\n" + 
				"          ELSE D.hid1\n" + 
				"END, distance\n" + 
				"FROM \"Distance\" AS D\n" + 
				"WHERE ((hid1 IN (SELECT a FROM cluster_center) AND hid2 IN (SELECT b\n" + 
				"FROM cluster_members)) OR (hid1 IN (SELECT b FROM cluster_members) AND\n" + 
				"hid2 IN (SELECT a FROM cluster_center)))\n" + 
				"AND D.hid1 <> D.hid2;";
		rsCor = this.connection.createStatement(
				ResultSet.TYPE_SCROLL_INSENSITIVE, 
				ResultSet.CONCUR_READ_ONLY).executeQuery(queryCoorCluster);					
		rsCor.last();
		
		int coor[][] = new int[rsCor.getRow()][3];
		rsCor.beforeFirst();
		int x1,y1;
		double d,dq,dx,dy;
		while (rsCor.next()) {
			d= rsCor.getDouble(2)*HashtagCoordinate.SCALE;
			dq = d*d;
			x1=clusterX;
			y1=clusterY;
			dy=(int)Math.round((Math.random()*d)); //d > dy+dx
			dx=Math.sqrt(dq-(dy*dy));
			coor[rsCor.getRow()-1][0]= (Math.random() > 0.5)?x1-(int)dx : x1+(int)dx;
			coor[rsCor.getRow()-1][1]= (Math.random() > 0.5)?y1-(int)dy : y1+(int)dy;
			coor[rsCor.getRow()-1][2]=rsCor.getInt(1);
		}
		rsCor.first();
		String insertQuery;
		for (int i=0;i<coor.length;i++) {
			insertQuery = "INSERT INTO \"Coordinate\" VALUES ("+coor[i][2]+","+coor[i][0]+","+coor[i][1]+","+centerHashtagId+")";
			this.connection.createStatement().execute(insertQuery);
			System.out.println(insertQuery);
		}
		
	}
	/**
	 * Creates 16 fixed coordinates for the 16 cluster centers. The coordinate positions of the cluster
	 * centers are hard coded and needs to be changed if more or less cluster centers are choosen. 
	 * @throws SQLException
	 */
	private void createClusterCoordinates() throws SQLException{
		String queryCluster = "SELECT * FROM \"Clustercenter\" ORDER BY cluster_id ASC;";
		ResultSet rs;
		rs = this.connection.createStatement(
				ResultSet.TYPE_SCROLL_INSENSITIVE, 
				ResultSet.CONCUR_READ_ONLY).executeQuery(queryCluster);					
		rs.last();
		
		int coor[][] = new int[rs.getRow()][4];
		rs.beforeFirst();
		//setting fixed start points for cluster
		coor[0][0]=-500;
		coor[0][1]=-500;
	
		coor[1][0]=-250;
		coor[1][1]=-500;
		
		coor[2][0]= 0;
		coor[2][1]=-500;
		
		coor[3][0]=250;
		coor[3][1]=-500;
	
		coor[4][0]=250;
		coor[4][1]=-250;
		
		coor[5][0]=-500;
		coor[5][1]=-250;
		
		coor[6][0]=-500;
		coor[6][1]=0;
		
		coor[7][0]=-500;
		coor[7][1]=250;
		
		coor[8][0]=250;
		coor[8][1]=0;
		
		coor[9][0]=-250;
		coor[9][1]=-250;
		
		coor[10][0]=-250;
		coor[10][1]=-0;
		
		coor[11][0]=-250;
		coor[11][1]=250;
		
		coor[12][0]=250;
		coor[12][1]=250;
		
		coor[13][0]=0;
		coor[13][1]=-250;
		
		coor[14][0]=0;
		coor[14][1]=0;
		
		coor[15][0]=0;
		coor[15][1]=250;
		
		while (rs.next()) {
			coor[rs.getRow()-1][2]=rs.getInt(2);
			coor[rs.getRow()-1][3]=rs.getInt(2);
		}
		rs.first();
		coor[0][2]=rs.getInt(2);
		String insertQuery;
		for (int i=0;i<coor.length;i++) {
			insertQuery = "INSERT INTO \"Coordinate\" VALUES ("+coor[i][2]+","+coor[i][0]+","+coor[i][1]+","+coor[i][3]+")";
			this.connection.createStatement().execute(insertQuery);
			System.out.println(insertQuery);
		}
		System.out.println("Cluster center wurden erfolgreich gesetzt.");

	}
	Connection connectDB(){
		Connection conn = null;
		try{
			Class.forName("org.postgresql.Driver").newInstance();
			String url = "jdbc:postgresql://"+HashtagCoordinate.DB_HOST+":"
							+HashtagCoordinate.DB_PORT+"/"+HashtagCoordinate.DB_NAME;
			Properties props = new Properties();
			props.setProperty("user", HashtagCoordinate.DB_USER);
			props.setProperty("password", HashtagCoordinate.DB_PASS);
	//		props.setProperty("ssl", "true");	Nicht geeignet für Abgabe
			conn = 	DriverManager.getConnection(url,props);
		}catch (Exception e){
			e.printStackTrace();
		}

		return conn;
	}


}
